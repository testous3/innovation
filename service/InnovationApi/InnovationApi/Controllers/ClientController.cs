using Innovation.Common.Entities;
using Innovation.Services.ClientService;
using Microsoft.AspNetCore.Mvc;

namespace InnovationApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClientController : ControllerBase
    {
        private readonly ILogger<ClientController> _logger;
        private readonly IClientService _clientService;

        public ClientController(ILogger<ClientController> logger, IClientService clientService)
        {
            _logger = logger;
            _clientService = clientService;
        }

        [HttpGet(Name = "GetClients")]
        public IEnumerable<Client> Get()
        {
            return _clientService.GetListeClients();
        }
    }
}