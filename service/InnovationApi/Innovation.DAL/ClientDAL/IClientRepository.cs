﻿using Innovation.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovation.DAL.ClientDAL
{
    public interface IClientRepository
    {
        List<Client> GetListeClients();
    }
}
