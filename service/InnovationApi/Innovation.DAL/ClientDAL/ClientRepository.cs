﻿using Innovation.Common.Entities;

namespace Innovation.DAL.ClientDAL
{
    public class ClientRepository : IClientRepository
    {
        private readonly List<Client> _listeClients = new()
        {
            new Client { Code = "ISAGRI", Nom = "GI", Prenom = "Imagination" },
            new Client { Code = "ISAGRI_KO", Nom = "K", Prenom = "O" },
            new Client { Code = "ISAGRI_GH", Nom = "G", Prenom = "H" }
        };

        public List<Client> GetListeClients()
        {
            return _listeClients;
        }
    }
}
