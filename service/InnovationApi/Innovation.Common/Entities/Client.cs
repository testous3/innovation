﻿namespace Innovation.Common.Entities
{
    public class Client
    {
        public string Code { get; set; }
        public string Nom { get; set; }
        public string Prenom { get; set; }
    }
}