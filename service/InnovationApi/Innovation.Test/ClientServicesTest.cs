using Innovation.Common.Entities;
using Innovation.DAL.ClientDAL;
using Innovation.Services.ClientService;
using Moq;

namespace Innovation.Test
{
    public class ClientServicesTest
    {
        [Fact]
        public void Test1()
        {
            var clientRepositoryMock = new Mock<IClientRepository>();
            var result = new List<Client>()
            {
            new Client() { Code = "TEST"}
            };
            clientRepositoryMock.Setup(s => s.GetListeClients()).Returns(result);

            var clientService = new ClientService(clientRepositoryMock.Object);

            var result2 = clientService.GetListeClients();

            Assert.True(result.Count == result2.Count);
            Assert.Equal(result.First().Code, result2.First().Code);
            Assert.Equal(result.First().Nom, result2.First().Nom);
        }
    }
}