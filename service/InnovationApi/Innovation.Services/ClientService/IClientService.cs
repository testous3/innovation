﻿using Innovation.Common.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Innovation.Services.ClientService
{
    public interface IClientService
    {
        List<Client> GetListeClients();
    }
}
